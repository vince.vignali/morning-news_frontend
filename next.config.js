/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['cdn.vox-cdn.com'],
  },
  env: {
    URL_API: process.env.URL_API,
  },
};

module.exports = nextConfig;
